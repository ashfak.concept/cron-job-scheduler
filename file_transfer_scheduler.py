import logging
import requests
from datetime import datetime
import time
import os

if __name__ == '__main__':
    # endpoint = 'http://www.google.com'
    endpoint = 'https://www.journolist.co.in/filetransfer.php'
    logfile = 'file_transfer.log'
    logging.basicConfig(filename=logfile, level=logging.INFO)
    while True:
        try:
            starttime = datetime.now()
            log_text = f"start time = {starttime} process id ={os.getpid()} endpoint={endpoint}"
            data = requests.get(endpoint)
            endtime = datetime.now()
            no_of_sec = (endtime-starttime).total_seconds()
            log_text = log_text + f" end_time = {endtime} execution time = {no_of_sec} seconds"
            logging.info(log_text)
            time.sleep(180)
        except Exception as e:
            logging.warning(f'[{datetime.now()}]:exception occured while hitting endpoint {endpoint} exception={e}')
            time.sleep(180)